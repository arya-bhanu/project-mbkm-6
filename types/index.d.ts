declare global {
  interface TopCampingIndonesiaType {
    place: string
    countNumber: number
    imgUrl: string
    id: number
  }
  interface CalendarButtonType {
    date: string
    dayWeek: string
    url: string
  }
  interface CampingGroundPopulerType {
    id: number
    lokasi: string
    rating: number
    title: string
    price: number
    status: string
    imgUrl: string
  }

  interface FasilitasPerkemahanType {
    title: string
  }

  interface FasilitasUmumType {
    title: string
  }

  interface PergiKesanaType {
    imgIcon: string
    vehicleType: string
    linkMap: string
    hourEstimation: {
      hourStart: number
      hourEnd: number
    }
  }

  enum VehicleType {
    Car,
    Walk
  }

  interface WisataTerdekatType {
    place: string
    reviewCount: number
    estimation: number
    starCount: number
    vehicleType: VehicleType
  }
  interface RestaurantTerdekatType {
    place: string
    starCount: number
    reviewCount: number
    estimation: number
    vehicleType: VehicleType
  }
  interface PaketType {
    id: number
    packetType: string
    benefit: string[]
    termsCondition: string[]
    price: {
      originalPrice: number
      discountPrice: number
    }
    include: string[]
    capacity: {
      from: number
      to?: number
    }
    imagesUrl: string[]
  }
  interface QuestionsAnswerType {
    id: number
    question: string
    answer: string
  }

  interface UlasanPengunjungTotal {
    percentage: number
    star: number
    ulasanCount: number
    starCount: {
      '5': 70
      '4': 50
      '3': 20
      '2': 0
      '1': 0
    }
  }

  interface UlasanPengunjungType {
    rating: number
    recomendStatus: string
    name: string
    statusTraveler: string
    pickedPacket: string
    comment: string
    profilePic: string
    imagesUrl: string[]
  }
}

export {
  TopCampingIndonesiaType,
  CalendarButtonType,
  CampingGroundPopulerType,
  FasilitasPerkemahanType,
  FasilitasUmumType,
  PergiKesanaType,
  VehicleType,
  RestaurantTerdekatType,
  WisataTerdekatType,
  PaketType,
  QuestionsAnswerType,
  UlasanPengunjungTotal,
  UlasanPengunjungType
}

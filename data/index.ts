import type {
  TopCampingIndonesiaType,
  CampingGroundPopulerType,
  FasilitasPerkemahanType,
  FasilitasUmumType,
  PergiKesanaType,
  RestaurantTerdekatType,
  WisataTerdekatType,
  PaketType,
  QuestionsAnswerType,
  UlasanPengunjungTotal,
  UlasanPengunjungType
} from '~/types'

// eslint-disable-next-line no-unused-vars
enum VehicleType {
  // eslint-disable-next-line no-unused-vars
  Car,
  // eslint-disable-next-line no-unused-vars
  Walk
}

export const ulasanPengunjung: UlasanPengunjungType[] = [
  {
    comment:
      'Tempatnya sangat indah dan damai. Berkemah di sini sangat menyenangkan, dengan pemandangan alam yang spektakuler dan suasana yang tenang.',
    imagesUrl: [
      '/assets/img/detail/pic-1.png',
      '/assets/img/detail/pic-2.png',
      '/assets/img/detail/pic-3.png',
      '/assets/img/detail/pic-4.png'
    ],
    name: 'Suwandi',
    pickedPacket: 'Paket Standard',
    profilePic: '/assets/img/detail/profpic.png',
    rating: 5,
    recomendStatus: 'Sangat Direkomendasikan',
    statusTraveler: 'Solo Traveler'
  },
  {
    comment:
      'Tempatnya sangat indah dan damai. Berkemah di sini sangat menyenangkan, dengan pemandangan alam yang spektakuler dan suasana yang tenang.',
    imagesUrl: [
      '/assets/img/detail/pic-1.png',
      '/assets/img/detail/pic-2.png',
      '/assets/img/detail/pic-3.png',
      '/assets/img/detail/pic-4.png'
    ],
    name: 'Suwandi',
    pickedPacket: 'Paket Standard',
    profilePic: '/assets/img/detail/profpic.png',
    rating: 3,
    recomendStatus: 'Sangat Direkomendasikan',
    statusTraveler: 'Solo Traveler'
  },
  {
    comment:
      'Tempatnya sangat indah dan damai. Berkemah di sini sangat menyenangkan, dengan pemandangan alam yang spektakuler dan suasana yang tenang.',
    imagesUrl: [
      '/assets/img/detail/pic-1.png',
      '/assets/img/detail/pic-2.png',
      '/assets/img/detail/pic-3.png',
      '/assets/img/detail/pic-4.png'
    ],
    name: 'Suwandi',
    pickedPacket: 'Paket Standard',
    profilePic: '/assets/img/detail/profpic.png',
    rating: 5,
    recomendStatus: 'Sangat Direkomendasikan',
    statusTraveler: 'Solo Traveler'
  }
]

export const ulasanPengunjungTotal: UlasanPengunjungTotal = {
  percentage: 92,
  star: 4,
  starCount: {
    '1': 0,
    '2': 0,
    '3': 20,
    '4': 50,
    '5': 70
  },
  ulasanCount: 150
}

export const questionsAnswers: QuestionsAnswerType[] = [
  {
    id: 1,
    question: 'Apa itu Bumi Perkemahan Ledok Ombo?',
    answer:
      'Bumi Perkemahan Ledok Ombo adalah tempat camping yang terletak di kaki Gunung Arjuno di Poncokusumo, Kabupaten Malang, Jawa Timur. Tempat ini menawarkan berbagai paket camping dan kegiatan outdoor lainnya.'
  },
  {
    id: 2,
    question: 'Apa saja fasilitas yang tersedia di Bumi Perkemahan Ledok Ombo?',
    answer:
      'Bumi Perkemahan Ledok Ombo adalah tempat camping yang terletak di kaki Gunung Arjuno di Poncokusumo, Kabupaten Malang, Jawa Timur. Tempat ini menawarkan berbagai paket camping dan kegiatan outdoor lainnya.'
  },
  {
    id: 3,
    question:
      'Apa saja kegiatan outdoor yang tersedia di Bumi Perkemahan Ledok Ombo?',
    answer:
      'Bumi Perkemahan Ledok Ombo adalah tempat camping yang terletak di kaki Gunung Arjuno di Poncokusumo, Kabupaten Malang, Jawa Timur. Tempat ini menawarkan berbagai paket camping dan kegiatan outdoor lainnya.'
  },
  {
    id: 4,
    question: 'Apakah tersedia paket camping untuk keluarga?',
    answer:
      'Bumi Perkemahan Ledok Ombo adalah tempat camping yang terletak di kaki Gunung Arjuno di Poncokusumo, Kabupaten Malang, Jawa Timur. Tempat ini menawarkan berbagai paket camping dan kegiatan outdoor lainnya.'
  },
  {
    id: 5,
    question: 'Apa saja paket yang ditawarkan di Bumi Perkemahan Ledok Ombo?',
    answer:
      'Bumi Perkemahan Ledok Ombo adalah tempat camping yang terletak di kaki Gunung Arjuno di Poncokusumo, Kabupaten Malang, Jawa Timur. Tempat ini menawarkan berbagai paket camping dan kegiatan outdoor lainnya.'
  }
]

export const paketWisata: PaketType[] = [
  {
    id: 1,
    benefit: [
      'Biaya Masuk & Parkir',
      'Peralatan Camping Dasar',
      'Akses Fasilitas Umum Perkemahan'
    ],
    capacity: {
      from: 2,
      to: 3
    },
    include: [
      'Tenda (230 cm x 160 cm)',
      'View Pegunungan',
      'Sleeping Bag (2)',
      'Matras (2)',
      'Senter (2)'
    ],
    packetType: 'standart',
    price: {
      discountPrice: 80000,
      originalPrice: 100000
    },
    termsCondition: ['Tidak Bisa Refund', 'Tidak Bisa Reschedule'],
    imagesUrl: [
      '/assets/img/detail/img-1.jpeg',
      '/assets/img/detail/img-2.jpeg',
      '/assets/img/detail/img-3.jpeg'
    ]
  },
  {
    id: 2,
    benefit: [
      'Biaya Masuk & Parkir',
      'Peralatan Camping Dasar',
      'Akses Fasilitas Umum Perkemahan'
    ],
    capacity: {
      from: 2,
      to: 3
    },
    include: [
      'Tenda (230 cm x 160 cm)',
      'View Pegunungan',
      'Sleeping Bag (2)',
      'Matras (2)',
      'Senter (2)'
    ],
    packetType: 'standart',
    price: {
      discountPrice: 80000,
      originalPrice: 100000
    },
    termsCondition: ['Tidak Bisa Refund', 'Tidak Bisa Reschedule'],
    imagesUrl: [
      '/assets/img/detail/img-1.jpeg',
      '/assets/img/detail/img-2.jpeg',
      '/assets/img/detail/img-3.jpeg'
    ]
  }
]

export const wisataTerdekat: WisataTerdekatType[] = [
  {
    estimation: 5,
    place: 'Coban Rondo Waterfall',
    reviewCount: 20,
    starCount: 5,
    vehicleType: VehicleType.Walk
  },
  {
    estimation: 1,
    place: 'Basecamp Opus Sunset',
    reviewCount: 10,
    starCount: 5,
    vehicleType: VehicleType.Car
  },
  {
    estimation: 1.5,
    place: 'Taman Kali Lesti',
    reviewCount: 30,
    starCount: 4,
    vehicleType: VehicleType.Car
  }
]

export const restaurantTerdekat: RestaurantTerdekatType[] = [
  {
    estimation: 5,
    place: 'Pinus Cafe',
    reviewCount: 20,
    starCount: 5,
    vehicleType: VehicleType.Walk
  },
  {
    estimation: 10,
    place: 'Restoran Pondok Alam',
    reviewCount: 15,
    starCount: 4,
    vehicleType: VehicleType.Walk
  },
  {
    estimation: 1,
    place: 'Warung Taman Makan',
    reviewCount: 10,
    starCount: 2,
    vehicleType: VehicleType.Car
  }
]

export const campingTopIndonesia: TopCampingIndonesiaType[] = [
  {
    countNumber: 236,
    place: 'malang',
    imgUrl: '/assets/slide/place-1.png',
    id: 1
  },
  {
    countNumber: 236,
    place: 'bandung',
    imgUrl: '/assets/slide/place-2.png',
    id: 2
  },
  {
    countNumber: 236,
    place: 'jakarta',
    imgUrl: '/assets/slide/place-3.png',
    id: 3
  },
  {
    countNumber: 236,
    place: 'yogyakarta',
    imgUrl: '/assets/slide/place-4.png',
    id: 4
  },
  {
    countNumber: 236,
    place: 'bogor',
    imgUrl: '/assets/slide/place-5.png',
    id: 5
  },
  {
    countNumber: 236,
    place: 'yogyakarta',
    imgUrl: '/assets/slide/place-4.png',
    id: 6
  }
]

export const campingGroundPopuler: CampingGroundPopulerType[] = [
  {
    id: 1,
    lokasi: 'Poncokusumo',
    price: 100000,
    title: 'SukaCita Camping Ground',
    rating: 4.0,
    status: 'Per Malam / Per Kavling',
    imgUrl: '/assets/img/poncokusumo.jpg'
  },
  {
    id: 2,
    lokasi: 'Poncokusumo',
    price: 100000,
    title: 'SukaCita Camping Ground',
    rating: 4.0,
    status: 'Per Malam / Per Kavling',
    imgUrl: '/assets/img/poncokusumo.jpg'
  },
  {
    id: 3,
    lokasi: 'Poncokusumo',
    price: 100000,
    title: 'SukaCita Camping Ground',
    rating: 4.0,
    status: 'Per Malam / Per Kavling',
    imgUrl: '/assets/img/poncokusumo.jpg'
  },
  {
    id: 4,
    lokasi: 'Poncokusumo',
    price: 100000,
    title: 'SukaCita Camping Ground',
    rating: 4.0,
    status: 'Per Malam / Per Kavling',
    imgUrl: '/assets/img/poncokusumo.jpg'
  },
  {
    id: 5,
    lokasi: 'Poncokusumo',
    price: 100000,
    title: 'SukaCita Camping Ground',
    rating: 4.0,
    status: 'Per Malam / Per Kavling',
    imgUrl: '/assets/img/poncokusumo.jpg'
  },
  {
    id: 6,
    lokasi: 'Poncokusumo',
    price: 100000,
    title: 'SukaCita Camping Ground',
    rating: 4.0,
    status: 'Per Malam / Per Kavling',
    imgUrl: '/assets/img/poncokusumo.jpg'
  },
  {
    id: 7,
    lokasi: 'Poo',
    price: 100000,
    title: 'SukaCita Camping Ground',
    rating: 4.0,
    status: 'Per Malam / Per Kavling',
    imgUrl: '/assets/img/poncokusumo.jpg'
  }
]

export const fasilitasPerkemahan: FasilitasPerkemahanType[] = [
  { title: 'Tenda' },
  { title: 'Kasur lipat' },
  { title: 'Sleeping bag' },
  { title: 'Bantal' },
  { title: 'Selimut' },
  {
    title: 'Meja dan kursi'
  }
]

export const fasilitasUmum: FasilitasUmumType[] = [
  {
    title: 'Toilet dan kamar mandi'
  },
  {
    title: 'Tempat parkir'
  },
  {
    title: 'Area BBQ'
  },
  {
    title: 'Penyewaan alat camping'
  },
  {
    title: 'Toko oleh-oleh'
  },
  {
    title: 'Resepsionis 24 jam'
  }
]

export const pergiKesana: PergiKesanaType[] = [
  {
    vehicleType: 'Mobil Pribadi',
    linkMap: '/',
    imgIcon: '/assets/vector/detail/car.svg',
    hourEstimation: { hourEnd: 2, hourStart: 1.5 }
  },
  {
    vehicleType: 'Minibus',
    linkMap: '/',
    imgIcon: '/assets/vector/detail/car.svg',
    hourEstimation: { hourEnd: 2, hourStart: 1.5 }
  },
  {
    vehicleType: 'Bus',
    linkMap: '/',
    imgIcon: '/assets/vector/detail/car.svg',
    hourEstimation: { hourEnd: 2, hourStart: 1.5 }
  }
]

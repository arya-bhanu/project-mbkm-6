/** @type {import('tailwindcss').Config} */
import defaultTheme from 'tailwindcss/defaultTheme'
export const content = [
  './components/**/*.{js,vue,ts}',
  './layouts/**/*.vue',
  './pages/**/*.vue',
  './plugins/**/*.{js,ts}',
  './nuxt.config.{js,ts}',
  './app.vue'
]
export const theme = {
  extend: {
    screens: {
      xs: '360px'
    },
    fontFamily: {
      inter: ['Inter', ...defaultTheme.fontFamily.sans],
      montserrat: ['Montserrat', ...defaultTheme.fontFamily.sans]
    },
    colors: {
      primary: '#064635',
      secondary: '#F1DAB3',
      'primary-dark': '#252525',
      'primary-darker': '#0D0D0D',
      'primary-white': '#F2F2F2',
      gold: '#F0BB62',
      'green-darker': '#3D3D3D',
      'primary-grey': '#6E6E6E',
      'secondary-green': '#0F5A46',
      'secondary-grey': '#565656'
    },
    container: {
      padding: {
        center: true,
        DEFAULT: '1rem'
      },
      screens: {
        xs: '100%'
      }
    }
  }
}
export const plugins = []
